class sparkSCIMUser {
    constructor(row, source) {
        if (source === 'spark') {
            this.id = row.id;
            this.externalID = row.id;
            this.userName = row.userName;
            this.emails = [{}];
            this.emails[0].value = row.userName;
            this.title = row.title;
            this.department = row.department;
            this.berthing = row.berthing;
            this.active = !row.disabled;
        }

        else {
            this.id = row.sparkID;
            this.externalID = row.sparkID;
            this.userName = row.userName;
            this.emails = [{}];
            this.emails[0].value = row.emails[0].value;
            this.active = row.active;
        }

        this.shipID = "SC";
        this.emails[0].primary = true;
        this.displayName = "none";
        this.nickName = 'none';
        this.profileUrl = 'none';
        this.name = {};
        this.name.givenName = row.Name;
        this.name.middleName = null;
        this.name.familyName = row.Name;
        this.emails[0].type = 'work';
        this.meta = {};
        this.meta.resourceType = 'user';
        this.meta.created = new Date();
        this.meta.lastMpdified = new Date();
        this.meta.location = new Date();
        this.schemas = ["urn:ietf:params:scim:schemas:core:2.0:User"];
    }
}

module.exports = sparkSCIMUser;