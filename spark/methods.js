class spark {
    disactivateSparkUsers(pin) {
        return new Promise(function(resolve,reject) {
            const request = require('request');
            const options = {
                url: `http://10.2.52.227/api/v1/ext/okta/user/${pin}`,
                headers: {"Authorization":"Bearer t80*d3yqfgf#t6$#u930vr77"},
            };
    
            request.delete(options, function(err,res) {
                if (err) {
                    reject(err);
                }
    
                else {
                    console.log(res.body);
                    resolve('ok');
                }
            });
        });
    }
    //Move Okta user data into Spark
    oktaToSpark(body, action) {
        return new Promise(function(resolve,reject) {
            const bcrypt = require('bcrypt');
            const saltRounds = 10;
            const request = require('request');
            const sparkSCIMUser = require('./sparkSCIMUser');
            if (action === 'insert') {
                //Generate hashed pass
                bcrypt.genSalt(saltRounds, function(err, salt) {
                    bcrypt.hash(body.password, salt, function(err, hash) {
                        const payload = {
                            "id": body.sparkID,
                            "userName": body.userName,
                            "title": body.title,
                            "shipId": "SC",
                            "department": body.department,
                            "berthing": body.berthing,
                            "password": hash
                        };
            
                        const options = {
                            url: "http://10.2.52.227/api/v1/ext/okta/user",
                            headers: {"Authorization":"Bearer t80*d3yqfgf#t6$#u930vr77","Content-Type":"application/json"},
                            body: JSON.stringify(payload)
                        };
                        //Insert user into Spark
                        request.post(options, function(err,res) {
                            if (err) {
                                reject(err);
                            }
            
                            else {
                                const resBody = JSON.parse(res.body);
                                if (resBody.code == 200) {
                                    let scim = new sparkSCIMUser(body, 'okta');
                                    resolve(scim);
                                }
            
                                else {
                                    reject(res.body);
                                }
                            }
                        });
                    });
                });
            }
            //Get spark user
            else if (action === 'get') {
                const options = {
                    url: "http://10.2.52.227/api/v1/ext/okta/user",
                    headers: {"Authorization":"Bearer t80*d3yqfgf#t6$#u930vr77"},
                };
    
                request.get(options, function(err,res) {
                    if (err) {
                        reject(err);
                    }
    
                    else {
                        const body = JSON.parse(res.body);
                        console.log(body);
                        if (body.code == 200) {
                            let scim = new sparkSCIMUser(body, 'okta');
                            resolve(scim);
                        }
    
                        else {
                            reject(res.body);
                        }
                    }
                });
            }
            //Insert updated user info into spark
            else if (action === 'update') {
                console.log(body);
                const payload = {
                    "id": body.sparkID,
                    "userName": body.userName,
                    "title": body.title,
                    "shipId": "SC",
                    "department": body.department,
                    "berthing": body.berthing
                };
    
                if (body.password) {
                    let salt = bcrypt.genSaltSync(saltRounds);
                    let hash = bcrypt.hashSync(body.password, salt);
                    payload.password = hash;
                }
    
                const options = {
                    url: `http://10.2.52.227/api/v1/ext/okta/user/${body.sparkID}`,
                    headers: {"Authorization":"Bearer t80*d3yqfgf#t6$#u930vr77","Content-Type":"application/json"},
                    body: JSON.stringify(payload)
                };
    
                request.put(options, function(err,res) {
                    if (err) {
                        reject(err);
                    }
    
                    else {
                        console.log('successfully put');
                        const resBody = JSON.parse(res.body);
                        if (resBody.code == 200) {
                            let scim = new sparkSCIMUser(body, 'okta');
                            resolve(scim);
                        }
    
                        else {
                            reject(res.body);
                        }
                    }
                });
            }
        });
    }
    //Move spark user data to okta
    sparkToOkta(filter, count, startIndex) {
        return new Promise(function(resolve,reject) {
            const request = require('request');
            const options = {
                url: "http://10.2.52.227/api/v1/ext/okta/user",
                headers: {"Authorization":"Bearer t80*d3yqfgf#t6$#u930vr77"},
            };
    
            request.get(options, function(err,res) {
                if (err) {
                    reject(err);
                }
    
                else {
                    const body = JSON.parse(res.body);
                    const users = body.users;
                    //Spark returns a list of all users, if we are searching for one user, lands here
                    if (filter) {
                        const target = users.find(function(e) {
                            return e[filter[0]] === filter[1];
                        });
                        //A user was found with the following filter
                        if (target) {
                            console.log("here w target: ", target);
                            resolve([target]);
                        }
                        //No user present
                        else {
                            resolve([]);
                        }
                        
                    }
    
                    else {
                        resolve(users);
                    }
                }
            });
        });
    }

    sparkToSCIM(rows, startIndex, count, app) {
        return new Promise(function(resolve) {
            const sparkSCIMUser = require('./sparkSCIMUser');
            let scimResource =  {
                "Resources": [], 
                "itemsPerPage": 0, 
                "schemas": [
                    "urn:ietf:params:scim:api:messages:2.0:ListResponse"
                ], 
                "startIndex": 0, 
                "totalResults": 0
            };
        
            let resources = [];
            
            if (count > rows.length) {
                count = rows.length;
            }
    
            for (let i = (startIndex-1); i < count; i++) {
                let scim = new sparkSCIMUser(rows[i], app);
                resources.push(scim);
            }
            scimResource.Resources = resources;
            scimResource.startIndex = startIndex;
            scimResource.itemsPerPage = count;
            scimResource.totalResults = count;
            resolve(JSON.stringify(scimResource));
        });
    }
}

module.exports = spark;