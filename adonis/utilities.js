class utilities {
    refreshAdonisToken() {
        return new Promise(function(resolve,reject) {
            const adonisConfig = require('./adonisConfig');
            const request = require('request');
            let apiConfig = require('../apiConfig.json');
            const options = {
                url: "http://10.198.0.20/AdonisWebServices_VirginVoyagesDEV/CrewPortalWebService.svc/GNL_API_AUTHENTICATION",
                headers: {"Content-Type":"application/json"},
                body: JSON.stringify(adonisConfig)
            };
    
            request.post(options, function(err,res) {
                const body = JSON.parse(res.body);
                if (err) {
                    reject(err);
                }
    
                else {
                    apiConfig.adonis.authToken = body.GNL_API_AUTHENTICATIONResult.Authentication_Token;
                    resolve(apiConfig.adonis.authToken);
                }
            });
        });
    }
}

module.exports = utilities;