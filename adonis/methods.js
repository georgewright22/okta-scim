class adonis {
    getAdonisUsers(filter) {
        const request = require('request');
        let apiConfig = require('../apiConfig.json');
        const utilitiesClass = require('./utilities');
        const utilities =  new utilitiesClass();
        return new Promise(function(resolve,reject) {
            let payloadBody = {
                "request": {
                    "Authentication_Token": apiConfig.adonis.authToken,
                    //Name of SQL view to query
                    "View":"crewprofile_v3"
                }
            };
    
            if (filter) {
              payloadBody.request.Filter = "email='" + filter[1] + "'";  
            }
    
            let options = {
                url: "http://10.198.0.20/AIWS_VirginVoyagesDEV/AdonisIntegrationWebService.svc/GNL_APMCrewListViews",
                headers: {"Content-Type":"application/json"},
                body: JSON.stringify(payloadBody)
            };
            //Call to Adonis for user data
            request.post(options, function(err,res) {
                const body = JSON.parse(res.body);
                
                if (err) {
                    reject(500, JSON.stringify(`{
                        "status": "500",
                        "response": {
                          "schemas": [
                            "urn:ietf:params:scim:api:messages:2.0:Error"
                          ],
                          "scimType": "serverError",
                          "detail": "${err}",
                          "status": "500"
                        }
                      }`));
                      return;
                }
    
                //token is expired
                else if (!body.GNL_APMCrewListViewsResult.Authentication_Approved) {
                    utilities.refreshAdonisToken().then(function(token) {
                        //update body with new auth token
                        payloadBody.request.Authentication_Token = token;
                        options.body = JSON.stringify(payloadBody);
                        //Call Adonis for user data
                        request.post(options, function(err,res) {
                            const body = JSON.parse(res.body);
                            if (err) {
                                reject(err);
                            }
                
                            else {
                                resolve(JSON.parse(body.GNL_APMCrewListViewsResult.Result));
                            }
                        });
                    });
                }
    
                else {
                    resolve(JSON.parse(body.GNL_APMCrewListViewsResult.Result));
                }
            });
        });
    }

    adonisToSCIM(rows, startIndex, count, app) {
        console.log('here', rows.length);
        return new Promise(function(resolve) {
            const SCIMUserObject = require('../SCIMUserObject');
            let scimResource =  {
                "Resources": [], 
                "itemsPerPage": 0, 
                "schemas": [
                    "urn:ietf:params:scim:api:messages:2.0:ListResponse"
                ], 
                "startIndex": 0, 
                "totalResults": 0
            };
        
            let resources = [];
            
            //Determine how many users to return
            if (count > rows.length) {
                count = rows.length;
            }

            for (let i = (startIndex-1); i < (rows.length < (parseInt(count) + parseInt(startIndex) - 1) ? rows.length : (parseInt(count) + parseInt(startIndex) - 1)); i++) {
                let scim = new SCIMUserObject(rows[i], app);
                resources.push(scim);
            }
            scimResource.Resources = resources;
            scimResource.startIndex = startIndex;
            scimResource.itemsPerPage = parseInt(count);
            scimResource.totalResults = parseInt(rows.length);
            resolve(JSON.stringify(scimResource));
        });
    }
}

module.exports = adonis;