class amos {
    amosToSCIM(rows, startIndex, count, app) {
        return new Promise(function(resolve) {
            const amosSCIMUser = require('./amosSCIMUser');
            let scimResource =  {
                "Resources": [], 
                "itemsPerPage": 0, 
                "schemas": [
                    "urn:ietf:params:scim:api:messages:2.0:ListResponse"
                ], 
                "startIndex": 0, 
                "totalResults": 0
            };
        
            let resources = [];
            
            if (count > rows.length) {
                count = rows.length;
            }
    
            for (let i = (startIndex-1); i < count; i++) {
                let scim = new amosSCIMUser(rows[i], app);
                resources.push(scim);
            }
            scimResource.Resources = resources;
            scimResource.startIndex = startIndex;
            scimResource.itemsPerPage = count;
            scimResource.totalResults = count;
            resolve(JSON.stringify(scimResource));
        });
    }
    //Get AMOS users and return to okta
    amosToOkta(filter, count, startIndex) {
        return new Promise(function(resolve,reject) {
            const sql = require("mssql");
            let query = '';
            if (filter) {
                query = "select * from amos.amos.amosuser where LoginID='" + filter[1] + "'";
            }
    
            else {
                query = "select * from amos.amos.amosuser";
            }
    
            let request = new sql.Request();
            request.query(query, function(err,records) {
                if (err) {
                    reject(err);
                }
        
                else {
                    resolve(records.recordsets[0]);
                }
            });
        }); 
    }
    //Move user data from okta into AMOS
    oktaToAmos(data, action, filter) {
        return new Promise(function(resolve,reject) {
            const sql = require("mssql");
            const amosSCIMUser = require('./amosSCIMUser');
            let query = '';
            //Build SQL query
            if (action === 'update') {
                query = `update amos.amos.amosuser set UserID = '${data.PIN}',LoginID = '${data.name.givenName}',Name = '${data.name.givenName}',Title = '${data.title}',Comment1 = '${data.email}',AccountDisabled = ${data.active ? 0 : 1},LastUpdated = '${moment().format('YYYYMMDD hh:mm:ss A')}',LastUpdatedUTC = '${moment().format('YYYYMMDD hh:mm:ss A')}',LastTouchedUTC = '${moment().format('YYYYMMDD hh:mm:ss A')}' where UserID='${data.PIN}'`;
            }
    
            else if (action === 'insert') {
                query = `insert into amos.amos.amosuser values(${data.PIN},${null},${null},${null},'${data.name.givenName}','${data.name.givenName}','${data.title}','${data.email}',${null},'${data.password}',${null},${data.active ? 0 : 1},${0},${0},${0},${0},'${moment().format('YYYYMMDD hh:mm:ss A')}','${moment().format('YYYYMMDD hh:mm:ss A')}','${moment().format('YYYYMMDD hh:mm:ss A')}','${moment().format('YYYYMMDD hh:mm:ss A')}')`;
            }
    
            else if (action === 'get') {
                query = `select * from amos.amos.amosuser where UserID='${filter[1]}'`;
            }
            let request = new sql.Request();
            request.query(query, function(err,records) {
                if (err) {
                    //user already exists in db but is inactive
                    if (err.originalError.info.message.indexOf('duplicate key') !== -1) {
                        //Re-activate user
                        request.query(`update amos.amos.amosuser set AccountDisabled=0 where UserID='${data.PIN}'`, function(err,records) {
                            if (err) {
                                reject(err);
                            }
    
                            else {
                                let scim = new amosSCIMUser(data);
                                resolve(scim);
                            }
                        });
                    }
    
                    else {
                        reject(err);
                    }
                }
        
                else {
                    let scim = '';
                    if (records.recordset) {
                        scim = new amosSCIMUser(records.recordset[0], 'amos');
                    }
    
                    else {
                        scim = new amosSCIMUser(data);
                    }
                    resolve(scim);
                }
            });
        });
    }

    disactivateAmosUsers(pin) {
        return new Promise(function(resolve,reject) {
            const sql = require("mssql");
            const amosSCIMUser = require('./amosSCIMUser');
            let query = `update amos.amos.amosuser set AccountDisabled=1 where UserID='${pin}'`;
            let request = new sql.Request();
            request.query(query, function(err) {
                if (err) {
                    reject(err);
                }
        
                else {
                    request.query(`select * from amos.amos.amosuser where UserID='${pin}'`, function(err,records) {
                        if (err) {
                            reject(err);
                        }
                
                        else {
                            let scim = new amosSCIMUser(records.recordset[0], 'amos');
                            resolve(scim);
                        }
                    });
                }
            });
        });
    }
}

module.exports = amos;