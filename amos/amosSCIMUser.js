class amosSCIMUser {
    constructor(row, source) {
        if (source === 'amos') {
            this.id = row.UserID;
            this.externalID = null;
            this.userName = row.Comment1;
            this.name = {};
            this.name.givenName = row.Name;
            this.name.middleName = null;
            this.name.familyName = row.Name;
            this.displayName = null;
            this.nickName = null;
            this.profileUrl = null;
            this.emails = [{}];
            this.emails[0].primary = true;
            this.emails[0].value = row.Comment1;
            this.emails[0].type = 'work';
            this.active = true;
        }

        else {
            this.id = row.PIN;
            this.externalID = row.externalId;
            this.userName = row.userName;
            this.name = {};
            this.name.givenName = row.name.givenName;
            this.name.middleName = 'empty';
            this.name.familyName = row.name.familyName;
            this.displayName = "none";
            this.nickName = 'none';
            this.profileUrl = 'none';
            this.emails = [{}];
            this.emails[0].primary = true;
            this.emails[0].value = row.email;
            this.emails[0].type = "work";
            this.active = true;
        }

        this.meta = {};
        this.meta.resourceType = 'user';
        this.meta.created = new Date();
        this.meta.lastMpdified = new Date();
        this.meta.location = new Date();
        this.schemas = ["urn:ietf:params:scim:schemas:core:2.0:User"];
    }
}

module.exports = amosSCIMUser;