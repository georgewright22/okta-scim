class apolloSCIMUser {
    constructor(row, source) {
        if (source === 'apollo') {
            this.id = row.loginId;
            this.name = {};
            this.name.givenName = row.name;
            this.name.familyName = row.name;
            this.name.middleName = row.name;
            this.emails = [{}];
            this.emails[0].value = row.email;
            this.lastModified = row.lastModified;
            this.validTo = row.validTo;
            this.validFrom = row.validFrom;
            this.active = row.isActive;
            this.userKey = row.userKey;
        }

        this.meta = {};
        this.meta.resourceType = 'user';
        this.meta.created = new Date();
        this.meta.lastMpdified = new Date();
        this.meta.location = new Date();
        this.schemas = ["urn:ietf:params:scim:schemas:core:2.0:User"];
    }
}

module.exports = apolloSCIMUser;