class apollo {
    apolloToSCIM(rows, startIndex, count, app) {
        return new Promise(function(resolve) {
            const apolloSCIMUser = require('./apolloSCIMUser');
            let scimResource =  {
                "Resources": [], 
                "itemsPerPage": 0, 
                "schemas": [
                    "urn:ietf:params:scim:api:messages:2.0:ListResponse"
                ], 
                "startIndex": 0, 
                "totalResults": 0
            };
        
            let resources = [];
            
            if (count > rows.length) {
                count = rows.length;
            }
    
            for (let i = (startIndex-1); i < count; i++) {
                let scim = new apolloSCIMUser(rows[i], app);
                resources.push(scim);
            }
            scimResource.Resources = resources;
            scimResource.startIndex = startIndex;
            scimResource.itemsPerPage = count;
            scimResource.totalResults = count;
            console.log('resource', scimResource);
            resolve(JSON.stringify(scimResource));
        });
    }

    disactivateApolloUsers(pin, activate) {
        return new Promise(function(resolve,reject) {
            const utilitiesClass = require('./utilities');
            const utilities = new utilitiesClass();

            const request = require('request');
            let apiConfig = require('../apiConfig.json');
            const rp = require('request-promise');
            const payload = {
                "applicationCode": "solution",
                "loginId": pin
            };
    
            const options = {
                //Determine which URL to hit based on whether we are activating or disactivating a user
                url: activate ? "https://apollosolutionlogintestapi.azurewebsites.net/api/Login/EnableAccount" : "https://apollosolutionlogintestapi.azurewebsites.net/api/Login/DisableAccount",
                headers: {"Authorization":`Bearer ${apiConfig.apollo.accessToken}`, "accept":"application/json","Content-Type":"application/json"},
                body: JSON.stringify(payload)
            };
            request.post(options, function(err,res) {
                if (err) {
                    console.log(err);
                    reject(err);
                }
    
                else {
                    //Auth failure
                    if (res.statusCode > 400) {
                        utilities.refreshApolloToken(apiConfig.apollo.refreshToken).then(function(accessToken) {
                            const options = {
                                method: 'POST',
                                uri: activate ? "https://apollosolutionlogintestapi.azurewebsites.net/api/Login/EnableAccount" : "https://apollosolutionlogintestapi.azurewebsites.net/api/Login/DisableAccount",
                                body: payload,
                                json: true,
                                headers: {"accept": "application/json", "Authorization": `Bearer ${accessToken}`}
                            };
                            //Make call again with new credentials 
                            rp(options)
                                .then(function (parsedBody) {
                                    console.log(parsedBody);
                                    resolve('');
                                })
                                .catch(function (err) {
                                    console.log('got an err');
                                    reject(err);
                                });
                        });
                    }
                    //call successful
                    else if (res.statusCode === 200) {
                        console.log(res.body);  
                        resolve('user');
                    }
                }
            });
        });
    }
    //Get user from Apollo and send it to Okta
    apolloToOkta(filter, count, startIndex) {
        return new Promise(function(resolve,reject) {
            const utilitiesClass = require('./utilities');
            const utilities = new utilitiesClass();
            let apiConfig = require('../apiConfig.json');
            const request = require('request');
            const options = {
                url: `https://apollosolutionlogintestapi.azurewebsites.net/api/Login/Solution/Users?email=${filter[1]}`,
                headers: {"Authorization":`Bearer ${apiConfig.apollo.accessToken}`}
            };
            console.log(options.url);
            //Get user
            request.get(options, function(err,res) {
                if (err) {
                    reject(err);
                }
    
                else {
                    //Invalid credentials
                    if (res.statusCode == 401) {
                        utilities.refreshApolloToken(apiConfig.apollo.refreshToken).then(function() {
                            const options = {
                                url: `https://apollosolutionlogintestapi.azurewebsites.net/api/Login/Solution/Users?email=${filter[1]}`,
                                headers: {"Authorization":`Bearer ${apiConfig.apollo.accessToken}`}
                            };                
                            request.get(options, function(err,res) {
                                console.log('apollo res', res.body);
                                if (err) {
                                    reject(err);
                                }
    
                                else if (res.statusCode == 400) {
                                    resolve([]);
                                }
                    
                                else {
                                    const payload = JSON.parse(res.body);
                                    resolve(payload);
                                }
                            });
                        });
                    }
                    //user doesnt exist
                    else if (res.statusCode == 400) {
                        resolve([]);
                    }
    
                    else {
                        console.log('apollo res1 first worked', res.body);
                        const payload = JSON.parse(res.body);
                        resolve(payload);
                    }
                }
            });
        });
    }
    //Send Okta user to Apollo
    oktaToApollo(user, action) {
        console.log('ere1234');
        return new Promise(function(resolve,reject) {
            const utilitiesClass = require('./utilities');
            const utilities = new utilitiesClass();
            const request = require('request');
            const rp = require('request-promise');
            let apiConfig = require('../apiConfig.json'); 
            const moment = require('moment');
            const apolloSCIMUser = require('./apolloSCIMUser');
            if (action === 'insert') {
                console.log('inserting');
                console.log(moment(user.validTo, 'YYYY-MM-DD HH:mm:ss').format('MM/DD/YYYY'));
                const payload = {
                    "applicationCode": "solution",
                    "name": `${user.name.givenName} ${user.name.middleName} ${user.name.familyName}`,
                    "loginId": user.loginId,
                    "password": user.password,
                    "email": user.emails[0].value,
                    "validUntil": moment(user.validTo, 'YYYY-MM-DD HH:mm:ss').format('MM/DD/YYYY')
                };
    
                const options = {
                    url: "https://apollosolutionlogintestapi.azurewebsites.net/api/Login/CreateAccount",
                    headers: {"Authorization":`Bearer ${apiConfig.apollo.accessToken}`, "accept":"application/json","Content-Type":"application/json"},
                    body: JSON.stringify(payload)
                };
                request.post(options, function(err,res) {
                    if (err) {
                        console.log('oktatoapollo', err);
                        reject(err);
                    }
        
                    else {
                        if (res.statusCode > 400) {
                            utilities.refreshApolloToken(apiConfig.apollo.refreshToken).then(function(accessToken) {
                                const options = {
                                    method: 'POST',
                                    uri: 'https://apollosolutionlogintestapi.azurewebsites.net/api/Login/CreateAccount',
                                    body: payload,
                                    json: true,
                                    headers: {"accept": "application/json", "Authorization": `Bearer ${accessToken}`}
                                };
                                 
                                rp(options)
                                    .then(function (parsedBody) {
                                        console.log('got a res');
                                        console.log(parsedBody);
                                        resolve(user);
                                    })
                                    .catch(function (err) {
                                        console.log('got a err');
                                        reject(err);
                                    });
                            });
                        }
    
                        else if (res.statusCode === 200) {
                            console.log(user);
                            user.id = user.loginId;  
                            console.log(res.body);  
                            resolve(user);
                        }
                    }
                });
            }
            //error is being thrown in this route if there is an issue with a conflicting PIN
            else if (action === 'update') {
                console.log('updating');
                console.log(user.validTo);
                console.log(moment(user.validTo, 'YYYY-MM-DD HH:mm:ss').format('MM/DD/YYYY'));
                const options = {
                    uri: `https://apollosolutionlogintestapi.azurewebsites.net/api/Login/Solution/User?Login%20Id=${user.loginId}`,
                    headers: {"Authorization":`Bearer ${apiConfig.apollo.accessToken}`},
                    json: true,
                    method: 'GET'
                };
                rp(options)
                    //successfully got user
                    .then(function (parsedBody) {
                        console.log('first call worked');
                        console.log('parsed', parsedBody);
                        console.log('user', user);
                        const putPayload = {
                            "applicationCode": "Solution",
                            "userKey": parsedBody.userKey,
                            "name": `${user.name.givenName} ${user.name.familyName}`,
                            "loginId": user.loginId,
                            "validFrom": user.validFrom,
                            "validTo": user.validTo,
                            "email": user.emails[0].value,
                            "isActive": parsedBody.isActive,
                            "lastModified": new Date()
                        };
                        
                        const putOptions = {
                            url: "https://apollosolutionlogintestapi.azurewebsites.net/api/Login/UpdateAccount",
                            headers: {"Authorization":`Bearer ${apiConfig.apollo.accessToken}`, "accept":"application/json","Content-Type":"application/json-patch+json"},
                            body: JSON.stringify(putPayload),
                            method: 'PUT'
                        };
                        rp(putOptions).then(function(parsedBody) {
                            console.log("all worked", parsedBody);
                            let scim = new apolloSCIMUser(putPayload, 'apollo');
                            console.log(scim);
                            resolve(scim);
                        }).catch(function(err) {
                            console.log("failed updating user:", err);
                        });
                    })
                    .catch(function (err) {
                        //initial call was unauthorized
                        if (err.statusCode == 500) {
                            utilities.refreshApolloToken(apiConfig.apollo.refreshToken).then(function(accessToken) {
                                console.log('got a ref token', accessToken);
                                //refreshed token
                                const refreshedOptions = {
                                    uri: `https://apollosolutionlogintestapi.azurewebsites.net/api/Login/Solution/User?Login%20Id=${user.loginId}`,
                                    headers: {"Authorization":`Bearer ${accessToken}`},
                                    json: true,
                                    method: 'GET'
                                };
                                rp(refreshedOptions)
                                    //second user get successful
                                    .then(function (parsedBody) {
                                        console.log('second call worked');
                                        console.log('parsed', parsedBody);
                                        console.log('user', user);
                                        const putPayload = {
                                            "applicationCode": "Solution",
                                            "userKey": parsedBody.userKey,
                                            "name": `${user.name.givenName} ${user.name.familyName}`,
                                            "loginId": user.loginId,
                                            "validFrom": user.validFrom,
                                            "validTo": user.validTo,
                                            "email": user.emails[0].value,
                                            "isActive": parsedBody.isActive,
                                            "lastModified": new Date()
                                        };
                                        
                                        const putOptions = {
                                            url: "https://apollosolutionlogintestapi.azurewebsites.net/api/Login/UpdateAccount",
                                            headers: {"Authorization":`Bearer ${apiConfig.apollo.accessToken}`, "accept":"application/json","Content-Type":"application/json-patch+json"},
                                            body: JSON.stringify(putPayload),
                                            method: 'PUT'
                                        };
                                        rp(putOptions).then(function(parsedBody) {
                                            console.log("all worked on second try", parsedBody);
                                            let scim = new apolloSCIMUser(putPayload, 'apollo');
                                            console.log(scim);
                                            resolve(scim);
                                        }).catch(function(err) {
                                            console.log("failed updating user:", err);
                                        });
                                    })
                                    .catch(function (err) {
                                        console.log("failed second call", err.StatusCodeError);
                                        //reject(err);
                                    });
                            });
                        }
    
                        else {
                            console.log("failed initial call", err);
                            reject(err);
                        }
                        
                    });
            }
        });
    }
}

module.exports = apollo;