class utilities {
    refreshApolloToken(refreshToken) {
        return new Promise(function(resolve,reject) {
            const rp = require('request-promise');
            let apiConfig = require('../apiConfig.json');
            if (!!refreshToken) {
                const options = {
                    method: 'POST',
                    uri: 'https://apollosolutionauthtestapi.azurewebsites.net/api/auth/Token/CompanyRefreshToken',
                    body: {
                        "clientId": apiConfig.apollo.clientID,
                        "clientSecret": apiConfig.apollo.clientSecret,
                        "companyCode": apiConfig.apollo.companyCode,
                        "refreshToken": apiConfig.apollo.refreshToken
                    },
                    json: true,
                    headers: {"accept": "application/json"}
                };
                 
                rp(options)
                    .then(function (parsedBody) {
                        console.log("body1", parsedBody);
                        apiConfig.apollo.refreshToken = parsedBody.refreshToken;
                        apiConfig.apollo.accessToken = parsedBody.accessToken;
                        resolve(parsedBody.accessToken);
                    })
                    .catch(function (err) {
                        console.log(err);
                    });
            }
        
            else {
                const options = {
                    method: 'POST',
                    uri: 'https://apollosolutionauthtestapi.azurewebsites.net/api/auth/Token/CompanyAuth',
                    body: {
                        "clientId": apiConfig.apollo.clientID,
                        "clientSecret": apiConfig.apollo.clientSecret,
                        "companyCode": apiConfig.apollo.companyCode
                    },
                    json: true,
                    headers: {"accept": "application/json"}
                };
                 
                rp(options)
                    .then(function (parsedBody) {
                        apiConfig.apollo.accessToken = parsedBody.accessToken;
                        apiConfig.apollo.refreshToken = parsedBody.refreshToken;
                        resolve(parsedBody.accessToken);
                    })
                    .catch(function (err) {
                        reject(err);
                    });
            }
        });
    }
}

module.exports = utilities;