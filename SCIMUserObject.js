class SCIMUserObject {
    constructor(row) {
        this.schemas = ["urn:ietf:params:scim:schemas:core:2.0:User"];
        this.id = row.teammemberNumber;
        this.userName = row.teamMemberUserName;
        this.name = {
            "givenName": row.firstName,
            "middleName": "",
            "familyName": row.lastName
        };
        this.emails = [{
            "primary": true,
            "type": "work",
            "value": row.email
        }];
        this.active = 1;
        this.groups = [];
        this.meta = {
            "resourceType": "User",
            "location": `/adonis/Users/${row.teammemberNumber}`
        };

        this.departmentKey = row.departmentKey;
        this.teammemberNumber = row.teammemberNumber;
        this.employee_company = row.employee_company;
        this.teamMemberUserName = row.teamMemberUserName;
        this.titleCode = row.titleCode;
        this.nickName = row.nickName;
        this.preferredName = row.preferredName;
        this.stateroom = row.stateroom;
        this.birthDate = row.birthDate;
        this.citizenshipCountrycode = row.citizenshipCountrycode;
        this.gender = row.gender;
        this.maritalStatus = row.maritalStatus;
        this.alternateEmail = row.alternateEmail;
        this.phoneTypeKey = row.phoneTypeKey;
        this.joiningDate = row.joiningDate;
        this.embarkDate = row.embarkDate;
        this.debarkDate = row.debarkDate;
        this.planned = row.planned;
        this.estimateSignOffDate = row.estimateSignOffDate;
        this.crewPhoto = row.crewPhoto;
        this.photoModifiedDate = row.photoModifiedDate;
        this.accessCardNumber = row.accessCardNumber;
        this.isActive = row.isActive;
        this.embarkPort = row.embarkPort;
        this.debarkPort = row.debarkPort;
        this.addressTypeKey = row.addressTypeKey;
        this.line1 = row.line1;
        this.line2 = row.line2;
        this.line3 = row.line3;
        this.city = row.city;
        this.state = row.state;
        this.addresscountryCode = row.addresscountryCode;
        this.zip = row.zip;
        this.countryCode = row.countryCode;
        this.ApolloPIN = row.ApolloPIN;
        this.VesselID = row.VesselID;
        this.Vessel = row.Vessel;
        this.DepartmentID = row.DepartmentID;
        this.Department = row.Department;
        this.PositionID = row.PositionID;
        this.Position = row.Position;
        this.employee_company_ID = row.employee_company_ID;
        this.employee_company_Code = row.employee_company_Code;
        this.c1d_visaTypeCode = row.c1d_visaTypeCode;
        this.c1d_visaNumber = row.c1d_visaNumber;
        this.c1d_issueCountryCode = row.c1d_issueCountryCode;
        this.c1d_issueDate = row.c1d_issueDate;
        this.c1d_expiryDate = row.c1d_expiryDate;
        this.b1b2_visaTypeCode = row.b1b2_visaTypeCode;
        this.b1b2_visaNumber = row.b1b2_visaNumber;
        this.b1b2_issueCountryCode = row.b1b2_issueCountryCode;
        this.b1b2_issueDate = row.b1b2_issueDate;
        this.b1b2_expiryDate = row.b1b2_expiryDate;
        this.schg_visaTypeCode = row.schg_visaTypeCode;
        this.schg_visaNumber = row.schg_visaNumber;
        this.schg_issueCountryCode = row.schg_issueCountryCode;
        this.schg_issueDate = row.schg_issueDate;
        this.schg_expiryDate = row.schg_expiryDate;
        this.dsbk_visaTypeCode = row.dsbk_visaTypeCode;
        this.dsbk_visaNumber = row.dsbk_visaNumber;
        this.dsbk_issueCountryCode = row.dsbk_issueCountryCode;
        this.dsbk_issueDate = row.dsbk_issueDate;
        this.dsbk_expiryDate = row.dsbk_expiryDate;
        this.smbk_visaTypeCode = row.smbk_visaTypeCode;
        this.smbk_visaNumber = row.smbk_visaNumber;
        this.smbk_issueCountryCode = row.smbk_issueCountryCode;
        this.smbk_issueDate = row.smbk_issueDate;
        this.smbk_expiryDate = row.smbk_expiryDate;
        this.pass_visaTypeCode = row.pass_visaTypeCode;
        this.pass_visaNumber = row.pass_visaNumber;
        this.pass_issueCountryCode = row.pass_issueCountryCode;
        this.pass_issueDate = row.pass_issueDate;
        this.pass_expiryDate = row.pass_expiryDate;
        this.passport_modifiedtime = row.passport_modifiedtime;
        this.isApproved = row.isApproved;
        this.Promotion = row.Promotion;
        this.safetyNo = row.safetyNo;
        this.charge_id = row.chargeID;
    }
}

module.exports = SCIMUserObject;