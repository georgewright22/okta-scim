
## Usage
1. Clone repo and `cd` into it

2. run `npm install`

3. run `node index` and server will start on port 7777

4. example call from new terminal window:
 `curl 'localhost:7777/Users?startIndex=1&count=100'`



----
## File Structure


**adonisConfig.json**

* Stores credentials used to get a new auth token from Adonis' API

**apiConfig.json**

* Store necessary tokens or configuration for accessing various application APIs

**config.json**

* Stores credentials to authenticate to MSSQL DBs

**SCIMUserObject.json**

* takes in a row from Adonis' API and transform it into a SCIM object ready to be sent back to Okta

**SCIMGroupObject.json**

* Currently unused but will serve the same purpose as the User Object