class mxp {
    mxpToOkta(filter) {
        return new Promise(function(resolve,reject) {
            const request = require('request');
            const options = {
                url: "http://10.198.0.134/API/MXP_Virgin.exe/crew",
                headers: {"Content-Type":"application/json"}
            };

            //get list of all mxp users
            //iterate through and find matching email stored n middle name field

            request.get(options, function(err,res) {
                if (err) {
                    reject(err);
                }
    
                else {
                    const users = JSON.parse(res.body);
                    //Spark returns a list of all users, if we are searching for one user, lands here
                    if (filter) {
                        const target = users.find(function(e) {
                            return e.middle_name === filter[1];
                        });
                        //A user was found with the following filter
                        if (target) {
                            console.log("here w target: ", target);
                            resolve([target]);
                        }
                        //No user present
                        else {
                            resolve([]);
                        }
                        
                    }
    
                    else {
                        resolve(users);
                    }
                }
            });
        });
    }

    mxpToSCIM(rows, startIndex, count, app) {
        return new Promise(function(resolve) {
            const mxpSCIMUser = require('./mxpSCIMUser');
            let scimResource =  {
                "Resources": [], 
                "itemsPerPage": 0, 
                "schemas": [
                    "urn:ietf:params:scim:api:messages:2.0:ListResponse"
                ], 
                "startIndex": 0, 
                "totalResults": 0
            };
        
            let resources = [];
            
            if (count > rows.length) {
                count = rows.length;
            }
    
            for (let i = (startIndex-1); i < count; i++) {
                let scim = new mxpSCIMUser(rows[i], app);
                resources.push(scim);
            }
            scimResource.Resources = resources;
            scimResource.startIndex = startIndex;
            scimResource.itemsPerPage = count;
            scimResource.totalResults = count;
            resolve(JSON.stringify(scimResource));
        });
    }

    oktaToMXP(body, action) {
        const util = require('util');
        const mxpSCIMUser = require('./mxpSCIMUser');
        const request = require('request');
        const shipLookup = {
            "Scarlet Lady": "SC"
        };
        if (action === 'insert') {
            return new Promise(function(resolve,reject) {
                const payload = {
                    "crew": [{
                        "charge_id": body.charge_id == undefined ? 0 : parseInt(body.charge_id),
                        "first_name": body.name.givenName,
                        "middle_name": body.middle_name == undefined ? "unknown" : body.middle_name,
                        "last_name": body.name.familyName,
                        "gender": body.gender == undefined ? "unknown" : body.gender,
                        "title": body.title == undefined ? "unknown" : body.title,
                        "place_of_birth": body.place_of_birth == undefined ? "unknown" : body.place_of_birth,
                        "date_of_birth": body.date_of_birth == undefined ? "1970-01-01" : body.date_of_birth,
                        "country_of_birth": body.country_of_birth == undefined ? "unknown" : body.country_of_birth,
                        "country_of_residence": body.country_of_residence == undefined ? "unknown" : body.country_of_residence,
                        "country_of_nationality": body.country_of_nationality == undefined ? "unknown" : body.country_of_nationality,
                        "marital_status_id": body.marital_status_id == undefined ? 6 : body.marital_status_id,
                        "is_person_active": body.is_person_active == undefined ? "unknown" : body.is_person_active,
                        "installation_code": shipLookup[body.installation_code] == undefined ? "unknown" : shipLookup[body.installation_code],
                        "room_nr": body.room_nr == undefined ? "0000" : body.room_nr,
                        "arrival_date": body.arrival_date == undefined ? "1970-01-01" : body.arrival_date,
                        "departure_date": body.departure_date == undefined ? "1970-01-01" : body.departure_date,
                        "is_checked_in": body.is_checked_in == undefined ? "unknown" : body.is_checked_in,
                        "PIN": body.PIN == undefined ? "unknown" : body.PIN,
                        "person_primary_email": body.emails == undefined ? "unknown" : body.emails[0].value,
                        "mxp_user": body.mxp_user == undefined ? "unknown" : body.mxp_user,
                        "position_name": body.position_name == undefined ? "unknown" : body.position_name,
                        "passport_type": body.passport_type == undefined ? 1 : body.passport_type,
                        "passport_number": body.passport_number == undefined ? "unknown" : body.passport_number,
                        "passport_first_name": body.passport_first_name == undefined ? "unknown" : body.passport_first_name,
                        "passport_last_name": body.passport_last_name == undefined ? "unknown" : body.passport_last_name,
                        "passport_issued_by": body.passport_issued_by == undefined ? "unknown" : body.passport_issued_by,
                        "passport_issued_country": body.passport_issued_country == undefined ? "unknown" : body.passport_issued_country,
                        "passport_issued_city": body.passport_issued_city == undefined ? "unknown" : body.passport_issued_city,
                        "passport_issued_date": body.passport_issued_date == undefined ? "1970-01-01" : body.passport_issued_date,
                        "passport_expiration_date": body.passport_expiration_date == undefined ? "1970-01-01" : body.passport_expiration_date,
                        "passport_is_primary": body.passport_is_primary == undefined ? false : body.passport_is_primary
                    }]
                };

                const options = {
                    url: "http://10.198.0.134/API/MXP_Virgin.exe/crew",
                    headers: {"Content-Type":"application/json"},
                    body: JSON.stringify(payload)
                };
                console.log(options);
                request.post(options, function(err, res) {
                    if (err) {
                        console.log(err);
                        reject(err);
                        return;
                    }
                    else {
                        const data = JSON.parse(res.body);
                        if (data.response_code == 'OK') {
                            const scim = new mxpSCIMUser(body);
                            resolve(scim);
                        }

                        else {
                            console.log('data',util.inspect(data, { maxArrayLength: null,showHidden: false, depth: null }));
                            reject(err);
                        }
                    }

                });
            });
        }
    }
}

module.exports = mxp;