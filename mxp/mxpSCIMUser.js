class mxpSCIMUser {
    constructor(row) {
        this.schemas = ["urn:ietf:params:scim:schemas:core:2.0:User"];
        this.id = row.PIN;
        this.userName = row.teamMemberUserName;
        this.name = {
            "givenName": row.first_name,
            "middleName": row.middle_name,
            "familyName": row.last_name
        };
        this.emails = [{
            "primary": true,
            "type": "work",
            "value": row.middle_name
        }];
        this.active = row.is_person_active;
        this.groups = [];
        this.meta = {
            "resourceType": "User",
            "location": `/mxp/Users/${row.PIN}`
        };

        this.charge_id = row.charge_id;
        /*this.first_name = row.first_name;
        this.middle_name = row.middle_name;
        this.last_name = row.last_name;*/
        this.gender = row.gender;
        this.title = row.title;
        this.place_of_birth = row.place_of_birth;
        this.date_of_birth = row.date_of_birth;
        this.country_of_birth = row.country_of_birth;
        this.country_of_residence = row.country_of_residence;
        this.country_of_nationality = row.country_of_nationality;
        this.marital_status_id = row.marital_status_id;
        this.is_person_active = row.is_person_active;
        this.installation_code = row.installation_code;
        this.room_nr = row.room_nr;
        this.arrival_date = row.arrival_date;
        this.departure_date = row.departure_date;
        this.is_checked_in = row.is_checked_in;
        this.person_id = row.person_id;
        this.PIN = row.PIN;
        this.mxp_user = row.mxp_user;
        this.username = row.username;
        this.position_name = row.position_name;
        this.department_name = row.department_name;
        this.passport_type = row.passport_type;
        this.passport_number = row.passport_number;
        this.passport_first_name = row.passport_first_name;
        this.passport_last_name = row.passport_last_name;
        this.passport_issued_by = row.passport_issued_by;
        this.passport_issued_country = row.passport_issued_country;
        this.passport_issued_city = row.passport_issued_city;
        this.passport_issued_date = row.passport_issued_date;
        this.passport_expiration_date = row.passport_expiration_date;
        this.passport_is_primary = row.passport_is_primary;
    }
}

module.exports = mxpSCIMUser;