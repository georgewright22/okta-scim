/* eslint-disable no-console */
const express = require('express');
const app = express();
const PORT = 7777;

let apiConfig = require('./apiConfig');
const sparkSCIMUser = require('./spark/sparkSCIMUser');
const apolloSCIMUser = require('./apollo/apolloSCIMUser');

/** Node Packages */
const request = require('request');
const util = require('util');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const rp = require('request-promise');

/**
 * Methods
 */
const apolloClass = require('./apollo/methods');
const apollo = new apolloClass();

const adonisClass = require('./adonis/methods');
const adonis = new adonisClass();

const amosClass = require('./amos/methods');
const amos = new amosClass();

const sparkClass = require('./spark/methods');
const spark = new sparkClass();

const mxpClass = require('./mxp/methods');
const mxp = new mxpClass();

//const mxpClass = require('./mxp/methods');
//const mxp = new mxpClass();

/** Connect to MSSQL DB 
const sql = require("mssql");
const config = apiConfig.amos.db;
const pool1 = new sql.ConnectionPool(config);
const pool1Connect = pool1.connect();
sql.connect(config, function (err) {
    if (err) {
        console.log(err);
    } 
});*/

/**
 * Parse okta payloads
 */
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ type: (req) => req.get('Content-Type') === 'application/scim+json; charset=utf-8' }));
app.use(bodyParser.json());

/**
 * Read a specific User based on PIN in Okta
 */
app.get('/:app/Users/:PIN', function(req, res) {
    const app = req.params.app;
    const PIN = req.params.PIN;
    const filter = ["PIN", PIN];

    if (app === 'amos') {
        amos.oktaToAmos(null, 'get', filter).then(SCIM => res.status(200).send(SCIM));
    }

    else if (app === 'adonis') {
        adonis.getAdonisUsers(filter).then(users => adonis.adonisToSCIM(users, 1, 1).then(SCIM => res.status(200).send(SCIM)));
    }

    else if (app === 'spark') {
        spark.sparkToOkta(["id",PIN]).then(users => spark.sparkToSCIM(users, 1, 1, 'spark').then(SCIM => res.status(200).send(SCIM)));
    }

    else if (app === 'apollo') {
        apollo.apolloToOkta(["id",PIN]).then(users => apollo.apolloToSCIM(users, 1, 1, 'apollo').then(SCIM => res.status(200).send(SCIM)));
    }

    else if (app === 'mxp') {
        console.log('1 mxp', req.url);
        console.log('1 mxp', req.body);
        //mxp.mxpToOkta(filter).then(users => mxp.mxpToSCIM(users, 1, 1, 'mxp').then(SCIM => res.status(200).send(SCIM)));
    }
});

/**
 * Read all Users
 */
app.get('/:app/Users', function(req, res) {
    console.log(req.url);
    const count = req.query.count || 999999;
    const startIndex = req.query.startIndex || 1;
    const app = req.params.app;
    let filter = req.query.filter;

    //If request contains a search filter, parse it
    if (filter) {
        filter = filter.split(" eq ");
        filter[1] = filter[1].replace(/\"/g, "");
    }

    if (app === "adonis") {
        adonis.getAdonisUsers(filter).then(users => adonis.adonisToSCIM(users, startIndex, count, app).then(function(SCIM) {
            res.status(200).send(SCIM);
        }));
    }
    
    else if (app === 'amos') {
        amos.amosToOkta(filter, count, startIndex).then(users => amos.amosToSCIM(users, startIndex, count).then(SCIM => res.status(200).send(SCIM)));
    }

    else if (app === 'spark') {
        spark.sparkToOkta(filter, count, startIndex).then(users => spark.sparkToSCIM(users, startIndex, count, 'spark').then(SCIM => res.status(200).send(SCIM)));
    }

    else if (app === "apollo") {
        console.log('getting apollo');
        apollo.apolloToOkta(filter, count, startIndex).then(users => apollo.apolloToSCIM(users, startIndex, count, 'apollo').then(SCIM => res.status(200).send(SCIM)));
    }

    else if (app === 'mxp') {
        console.log('2 mxp', req.url);
        console.log('2 mxp', req.body);
        mxp.mxpToOkta(filter).then(users => mxp.mxpToSCIM(users, 1, 1, 'mxp').then(function(SCIM) {
            console.log('mxp scim res', SCIM);
            res.status(200).send(SCIM);
        }));
    }

    else {
        res.status(200).send('{"Resources":[],"itemsPerPage":0,"schemas":["urn:ietf:params:scim:api:messages:2.0:ListResponse"],"startIndex":0,"totalResults":0}');
    }
});

/**
 * Okta sends a POST here if a User 
 * is manually created in Okta
 */
app.post('/:app/Users', function(req, res) {
    let app = req.params.app;
    if (app === 'amos') {
        amos.oktaToAmos(req.body, 'insert').then(SCIM => res.status(200).send(SCIM));
    }

    else if (app === 'spark') {
        spark.oktaToSpark(req.body, 'insert').then(SCIM => res.status(200).send(SCIM));
    }
    else if (app === 'apollo') {
        apollo.oktaToApollo(req.body, 'insert').then(function(SCIM) {
            console.log(SCIM);
            res.status(200).send(SCIM);
        });
    }

    else if (app === 'mxp') {
        mxp.oktaToMXP(req.body, 'insert').then(function(SCIM) {
            console.log(SCIM);
            res.status(200).send(SCIM);
        });
    }

    return;
});

/**
 * Update a User
 */
app.put('/:app/Users/:id', function(req,res) {
    console.log('updating a user');
    const app = req.params.app;
    if (app === 'amos') {
        amos.oktaToAmos(req.body, 'update', null).then(SCIM => res.status(200).send(SCIM));
    }

    else if (app === 'spark') {
        spark.oktaToSpark(req.body, 'update').then(SCIM => res.status(200).send(SCIM));
    }

    else if (app === 'apollo') {
        apollo.oktaToApollo(req.body, 'update').then(function(SCIM) {
            console.log(SCIM);
            res.status(200).send(SCIM);
        });
    }

    else if (app === 'mxp') {
        console.log('4 mxp', req.url);
        console.log('4 mxp', req.body);
        //mxp.mxpToOkta(filter).then(users => mxp.mxpToSCIM(users, 1, 1, 'mxp').then(SCIM => res.status(200).send(SCIM)));
    }
});

/**
 * Delete a User
 */
app.patch('/:app/Users/:PIN', function(req, serverRes) {
    console.log('jere');
    console.log(req.url);
    console.log('body',util.inspect(req.body, { maxArrayLength: null,showHidden: false, depth: null }));

    const app = req.params.app;

    if (app === 'amos') {
        amos.disactivateAmosUsers(req.params.PIN);
    }

    else if (app === 'spark') {
        //User is being re-activated
        if (req.body.Operations[0].value.active) {
            const options = {
                url: "http://10.2.52.227/api/v1/ext/okta/user",
                headers: {"Authorization":"Bearer t80*d3yqfgf#t6$#u930vr77"},
            };
            //Retrieve disabled user profile from Spark
            request.get(options, function(err,res) {
                if (err) {
                    console.log("first err", err);
                    return err;
                }

                else {
                    const body = JSON.parse(res.body);
                    if (body.code == 200) {
                        //Build user payload so we can post it back to Spark and re-enable the account
                        body.users.map(function(user) {
                            if (user.id == req.params.PIN) {
                                const payload = {
                                    "id": user.id,
                                    "userName": user.userName,
                                    "title": user.title,
                                    "shipId": user.shipId,
                                    "disabled": false,
                                    "department": user.department,
                                    "berthing": user.berthing
                                };
                                const options = {
                                    url: `http://10.2.52.227/api/v1/ext/okta/user/${user.id}`,
                                    headers: {"Authorization":"Bearer t80*d3yqfgf#t6$#u930vr77", "Content-Type": "application/json"},
                                    body: JSON.stringify(payload)
                                };
                    
                                request.put(options, function(err,res) {
                                    if (err) {
                                        console.log("first err", err);
                                        return err;
                                    }
                    
                                    else {
                                        //Send new user back to okta
                                        let scim = new sparkSCIMUser(user, 'spark');
                                        serverRes.status(200).send(scim);
                                    }
                                });
                            }
                        });
                    }

                    else {
                        console.log("Err", res.body);
                        return err;
                    }
                }
            });
        }

        else if (req.body.Operations[0].value.password) {
            const options = {
                url: "http://10.2.52.227/api/v1/ext/okta/user",
                headers: {"Authorization":"Bearer t80*d3yqfgf#t6$#u930vr77"},
            };
            //Retrieve disabled user profile from Spark
            request.get(options, function(err,res) {
                if (err) {
                    console.log("first err", err);
                    return err;
                }

                else {
                    const body = JSON.parse(res.body);
                    if (body.code == 200) {
                        //Build user payload so we can post it back to Spark and re-enable the account
                        body.users.map(function(user) {
                            if (user.id == req.params.PIN) {
                                bcrypt.genSalt(saltRounds, function(err, salt) {
                                    bcrypt.hash(req.body.Operations[0].value.password, salt, function(err, hash) {
                                        const payload = {
                                            "id": user.id,
                                            "userName": user.userName,
                                            "title": user.title,
                                            "shipId": user.shipId,
                                            "disabled": false,
                                            "department": user.department,
                                            "berthing": user.berthing,
                                            "password": hash
                                        };
                                        const options = {
                                            url: `http://10.2.52.227/api/v1/ext/okta/user/${user.id}`,
                                            headers: {"Authorization":"Bearer t80*d3yqfgf#t6$#u930vr77", "Content-Type": "application/json"},
                                            body: JSON.stringify(payload)
                                        };
                                        console.log(' here w password', payload);
                                        request.put(options, function(err,res) {
                                            if (err) {
                                                console.log("first err", err);
                                                return err;
                                            }
                            
                                            else {
                                                //Send new user back to okta
                                                let scim = new sparkSCIMUser(user, 'spark');
                                                serverRes.status(200).send(scim);
                                            }
                                        });
                                    });
                                });
                            }
                        });
                    }

                    else {
                        console.log("Err", res.body);
                        return err;
                    }
                }
            });
        }

        else {
            spark.disactivateSparkUsers(req.params.PIN);
        }
        
    }
    else if (app === 'apollo') {
        //reactivate inactive user
        if (req.body.Operations[0].value.active) {
            apollo.disactivateApolloUsers(req.params.PIN, true).then(function() {
                const options = {
                    url: `https://apollosolutionlogintestapi.azurewebsites.net/api/Login/Solution/User?Login%20Id=${req.params.PIN}`,
                    headers: {"Authorization":`Bearer ${apiConfig.apollo.accessToken}`}
                };
                request.get(options, function(err,res) {
                    if (err) {
                        console.log(err);
                    }
        
                    else {
                        apollo.apolloToSCIM([JSON.parse(res.body)], 1, 1, 'apollo').then(SCIM => serverRes.status(200).send(SCIM));
                    }
                });
            });
        }

        else if (req.body.Operations[0].value.password) {
            console.log(apiConfig);
            return new Promise(function(resolve,reject) {
                const utilitiesClass = require('./apollo/utilities');
                const utilities = new utilitiesClass();
                console.log('updating apollo password');
                const options = {
                    uri: `https://apollosolutionlogintestapi.azurewebsites.net/api/Login/Solution/User?Login%20Id=${req.params.PIN}`,
                    headers: {"Authorization":`Bearer ${apiConfig.apollo.accessToken}`},
                    json: true,
                    method: 'GET'
                };
                console.log(options);
                rp(options)
                    //successfully got user
                    .then(function (parsedBody) {
                        console.log('first call worked');
                        console.log('parsed', parsedBody);
                        const putPayload = {
                            "applicationCode": "Solution",
                            "userKey": parsedBody.userKey,
                            "name": parsedBody.name,
                            "loginId": parsedBody.loginId,
                            "validFrom": parsedBody.validFrom,
                            "validTo": parsedBody.validTo,
                            "email": parsedBody.email,
                            "isActive": parsedBody.isActive,
                            "lastModified": new Date(),
                            "password": req.body.Operations[0].value.password
                        };
                        
                        const putOptions = {
                            url: "https://apollosolutionlogintestapi.azurewebsites.net/api/Login/UpdateAccount",
                            headers: {"Authorization":`Bearer ${apiConfig.apollo.accessToken}`, "accept":"application/json","Content-Type":"application/json-patch+json"},
                            body: JSON.stringify(putPayload),
                            method: 'PUT'
                        };
                        rp(putOptions).then(function(parsedBody) {
                            console.log("all worked", parsedBody);
                            let scim = new apolloSCIMUser(putPayload, 'apollo');
                            console.log(scim);
                            resolve(scim);
                        }).catch(function(err) {
                            console.log("failed updating user:", err);
                        });
                    })
                    .catch(function (err) {
                        //initial call was unauthorized
                        if (err.statusCode == 500 || err.statusCode == 401 ) {
                            utilities.refreshApolloToken(apiConfig.apollo.refreshToken).then(function(accessToken) {
                                console.log('got a ref token', accessToken);
                                //refreshed token
                                const refreshedOptions = {
                                    uri: `https://apollosolutionlogintestapi.azurewebsites.net/api/Login/Solution/User?Login%20Id=${req.params.PIN}`,
                                    headers: {"Authorization":`Bearer ${accessToken}`},
                                    json: true,
                                    method: 'GET'
                                };
                                rp(refreshedOptions)
                                    //second user get successful
                                    .then(function (parsedBody) {
                                        console.log('second call worked');
                                        console.log('parsed', parsedBody);
                                        const putPayload = {
                                            "applicationCode": "Solution",
                                            "userKey": parsedBody.userKey,
                                            "name": parsedBody.name,
                                            "loginId": parsedBody.loginId,
                                            "validFrom": parsedBody.validFrom,
                                            "validTo": parsedBody.validTo,
                                            "email": parsedBody.email,
                                            "isActive": parsedBody.isActive,
                                            "lastModified": new Date(),
                                            "password": req.body.Operations[0].value.password
                                        };
                                        
                                        const putOptions = {
                                            url: "https://apollosolutionlogintestapi.azurewebsites.net/api/Login/UpdateAccount",
                                            headers: {"Authorization":`Bearer ${apiConfig.apollo.accessToken}`, "accept":"application/json","Content-Type":"application/json-patch+json"},
                                            body: JSON.stringify(putPayload),
                                            method: 'PUT'
                                        };
                                        rp(putOptions).then(function(parsedBody) {
                                            console.log("all worked on second try", parsedBody);
                                            let scim = new apolloSCIMUser(putPayload, 'apollo');
                                            console.log(scim);
                                            resolve(scim);
                                        }).catch(function(err) {
                                            console.log("failed updating user:", err);
                                        });
                                    })
                                    .catch(function (err) {
                                        console.log("failed second call", err.StatusCodeError);
                                        //reject(err);
                                    });
                            });
                        }

                        else {
                            console.log("failed initial call", err);
                            reject(err);
                        }
                        
                    });
                });
        }
        //delete user
        else {
            apollo.disactivateApolloUsers(req.params.PIN, false);
        }
    }
});

/** 
 * Send back an empty resource array 
 * as we have no groups 
 */
app.get('/:app/Groups', function(req, res) {
	console.log(req.url);
	res.status(200).send('{"Resources":[],"schemas":["urn:ietf:params:scim:api:messages:2.0:ListResponse"],"startIndex":1,"totalResults":0}%');
});

app.listen(PORT, function(err) {
    if (err) {
        console.log(err);
    }

    else {
        console.log(`listening on ${PORT}`);
    }
}); 